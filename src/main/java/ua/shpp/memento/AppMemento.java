package ua.shpp.memento;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.classes.Git;
import ua.shpp.classes.IdeaProject;

public class AppMemento {
    private static final Logger log = LoggerFactory.getLogger(AppMemento.class);
    public static void main(String[] args) {
        IdeaProject ideaProject = new IdeaProject("first commit", 15);
        log.info("{}",ideaProject);

        Git git = new Git();
        git.setGitObject(ideaProject.addCommit());

        ideaProject.setValues("Refactoring", 10);
        log.info("{}",ideaProject);
        git.setGitObject(ideaProject.addCommit());
        ideaProject.load(git.getGitObject(0));
        log.info("{}",ideaProject);
    }
}