package ua.shpp.classes;

public class GitObject {
    private final String commit;
    private final int numberOfLines;

    public GitObject(String commit, int numberOfLines) {
        this.commit = commit;
        this.numberOfLines= numberOfLines;
    }

    public String getCommit() {
        return commit;
    }

    public int getNumberOfLines() {
        return numberOfLines;
    }
}
