package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdeaProject {
    private final Logger log = LoggerFactory.getLogger(IdeaProject.class);
    private String commit;
    private int numberOfLines;

    public IdeaProject(String commit, int numberOfLines) {
        this.commit = commit;
        this.numberOfLines = numberOfLines;
    }
    public void setValues(String commit, int numberOfLines){
        this.commit = commit;
        this.numberOfLines = numberOfLines;
    }
    public void load(GitObject gitObject) {
        this.commit = gitObject.getCommit();
        this.numberOfLines = gitObject.getNumberOfLines();
    }
    public GitObject addCommit(){
        return new GitObject(commit,numberOfLines);
    }

    @Override
    public String toString() {
        return "IdeaProject{" +
                "commit='" + commit + '\'' +
                ", numberOfLines=" + numberOfLines +
                '}';
    }
}
