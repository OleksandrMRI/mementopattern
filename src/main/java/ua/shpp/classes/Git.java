package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Git {
    Logger log = LoggerFactory.getLogger(Git.class);
    private ArrayList<GitObject> list;

    public Git() {
        this.list = new ArrayList<>();
    }

    public GitObject getGitObject(int numberOfCommit) {
        return list.get(numberOfCommit);
    }

    public void setGitObject(GitObject gitObject) {
        list.add(gitObject);
    }
}
